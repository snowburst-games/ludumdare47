// GameSettingsData object: we wrap all our data in a convenient object to serialise/deserialize json
using System.Collections.Generic;
using System;

public class GameSettingsData : IJSONSaveable
{
    public Dictionary<int, string> PlayerNames;
    public Dictionary<string, float> SoundVolume;
    public Dictionary<GameSettings.Controls, string> InputEvents;
}