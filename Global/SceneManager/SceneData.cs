// Scene data: contains references to all Stage scenes within the game - for simplicity so we don't have to update path in each script.

using System;

public class SceneData
{
    public enum Stage
    {
        MainMenu,
        World
    }

    public enum Scene
    {
        Spice,
        Obstacle
    }

    public readonly System.Collections.Generic.Dictionary<Stage,string> Stages 
        = new System.Collections.Generic.Dictionary<Stage, string>()
    {
        {Stage.MainMenu, "res://Stages/MainMenu/StageMainMenu.tscn"},
        {Stage.World, "res://Stages/World/StageWorld.tscn"}
    };

    public static System.Collections.Generic.Dictionary<Scene,string> Scenes 
        = new System.Collections.Generic.Dictionary<Scene, string>()
    {
        // {Scene.NetState, "res://Global/NetState/NetState.tscn"},
        // {Scene.Lobby, "res://Utils/Network/Lobby/Lobby.tscn"},
        // {Scene.Player, "res://Entities/Characters/Player/Player.tscn"},
        // {Scene.Creature, "res://Entities/Creature/Creature.tscn"},
        {Scene.Spice, "res://Props/Spice/Spice.tscn"},
        {Scene.Obstacle, "res://Doodads/Obstacle/Obstacle.tscn"}
    };
}
