// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private Dictionary<AudioData.MainMenuSounds, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSounds>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		GetNode<ScoreManagerMono>("ScoreManagerMono").RefreshLeaderboard(10);
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSounds.Music], AudioData.SoundBus.Music);
		_popAbout.Visible = false;
		// Load settings at start if they exist. This should run at startup so if another scene is set at startup put this there.
		// GameSettings.Instance.LoadFromJSON();
		GetNode<Button>("BtnToggleMute").Text = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master")) ? "UNMUTE" : "MUTE";

	}
	private void Intro()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/INTRO_PICTURE_STORY.png", 
		@"");
		_pictureStory.Start();
		
	}

	private void OnIntroFinished()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	private void OnBtnPlayPressed()
	{
		Intro();
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}

	private void _on_BtnQuit_pressed()
	{
		GetTree().Quit();
	}

	private void _on_BtnLeaderboard_pressed()
	{
		GetNode<ScoreManagerMono>("ScoreManagerMono").ShowLeaderboard();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = muted ? "MUTE" : "UNMUTE";
		GetNode<Button>("BtnToggleMute").Text = label;
	}
	
	private void _on_LinkButton_pressed()
	{
		OS.ShellOpen("https://sage7.itch.io/");
	}

}




