using Godot;
using System;

public class SpiceGenerator : Node2D
{
	public delegate void ConveyerSpeedChangedDelegate(float speed);
	public event ConveyerSpeedChangedDelegate ConveyerSpeedChanged;
	public Action<int> AddScore;
	private Timer _genTimer;
	private Random _rand = new Random();
	private PackedScene _spiceScn;
	public float[] YBounds {get; set;}
	private float _conveyerSpeed = 0;
	public float ConveyerSpeed
	{
		get{
			return _conveyerSpeed;
		}
		set{
			_conveyerSpeed = value;
			ConveyerSpeedChanged?.Invoke(_conveyerSpeed);
		}
	}

	public override void _Ready()
	{
		base._Ready();
		_genTimer = GetNode<Timer>("GenTimer");
		_spiceScn = GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.Spice]);
		StartGenTimer();
	}

	public void GenerateSpice()
	{
		if (YBounds == null)
		{
			return;
		}
		Spice spice = (Spice) _spiceScn.Instance();
		spice.SpiceType = (Spice.SpiceTypeEnum) new Random().Next(0, Enum.GetValues(typeof(Spice.SpiceTypeEnum)).Length);
		ConveyerSpeedChanged+=spice.OnConveyerSpeedChanged;
		spice.SpiceTouchedPlayer+=this.OnSpiceTouchedPlayer;
		AddChild(spice);
		spice.OnConveyerSpeedChanged(ConveyerSpeed);
		spice.SetRandomPosition(YBounds);
	}

	public void OnSpiceTouchedPlayer(int score)
	{
		AddScore(score);
	}

	private void OnGenTimerTimeout()
	{
		GenerateSpice();
		StartGenTimer();
	}

	private void StartGenTimer()
	{
		_genTimer.WaitTime = Math.Max(0.5f, _rand.Next(1,6) - ConveyerSpeed/100);
		_genTimer.Start();
	}

	public void UnsubscribeEvents()
	{
		ConveyerSpeedChanged = null;
	}

}

