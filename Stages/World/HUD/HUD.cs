using Godot;
using System;

public class HUD : CanvasLayer
{
	public delegate void BtnQuitPressedDelegate();
	public event BtnQuitPressedDelegate BtnQuitPressed;

	private Panel _pnlMenu;
	private Label _lblStart;
	private AnimationPlayer _anim;
	public bool Starting {get; set;} = true;
	

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_pnlMenu = GetNode<Panel>("PnlMenu");
		_lblStart = GetNode<Label>("LblStart");
		_anim = GetNode<AnimationPlayer>("Anim");
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	//  public override void _Process(float delta)
	//  {
	//      
	//  }

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);

		if (ev.IsActionPressed("Pause"))
		{
			// _pnlMenu.Visible = GetTree().Paused = !_pnlMenu.Visible;
			PauseToggle();
		}
	}

	public void PauseToggle()
	{
		if (Starting)
		{
			if (_pnlMenu.Visible)
			{
				OnBtnResumePressed();
			}
			else
			{
				OnBtnMenuPressed();
			}
			return;
		}
		_pnlMenu.Visible = GetTree().Paused = !_pnlMenu.Visible;
	}

	private void OnBtnMenuPressed()
	{
		// _pnlMenu.Visible = GetTree().Paused = true;
		if (Starting)
		{
			_pnlMenu.Visible = true;
			_anim.Stop(false);
			return;
		}
		PauseToggle();
	}

	private void OnBtnResumePressed()
	{
		// _pnlMenu.Visible = GetTree().Paused = false;
		if (Starting)
		{
			_pnlMenu.Visible = false;
			_anim.Play();
			return;
		}
		PauseToggle();
	}


	private void OnBtnQuitPressed()
	{
		BtnQuitPressed?.Invoke();
		BtnQuitPressed = null;
	}

}


