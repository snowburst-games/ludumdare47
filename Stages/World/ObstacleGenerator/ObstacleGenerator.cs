using Godot;
using System;

public class ObstacleGenerator : Node2D
{
	public delegate void ConveyerSpeedChangedDelegate(float speed);
	public event ConveyerSpeedChangedDelegate ConveyerSpeedChanged;
	public Action AddScore;
	private Timer _genTimer;
	private Random _rand = new Random();
	private PackedScene _obstacleScn;
	public float[] YBounds {get; set;}
	private float _conveyerSpeed = 0;
	public float ConveyerSpeed
	{
		get{
			return _conveyerSpeed;
		}
		set{
			_conveyerSpeed = value;
			ConveyerSpeedChanged?.Invoke(_conveyerSpeed);
		}
	}

	public override void _Ready()
	{
		base._Ready();
		_genTimer = GetNode<Timer>("GenTimer");
		_obstacleScn = GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.Obstacle]);
		StartGenTimer();
	}

	public void GenerateObstacle()
	{
		if (YBounds == null)
		{
			return;
		}
		Obstacle obstacle = (Obstacle) _obstacleScn.Instance();
		obstacle.ObstacleType = (Obstacle.ObstacleTypeEnum) new Random().Next(0, Enum.GetValues(typeof(Obstacle.ObstacleTypeEnum)).Length);
		ConveyerSpeedChanged+=obstacle.OnConveyerSpeedChanged;
		obstacle.ObstacleFell+=this.OnObstacleFell;
		AddChild(obstacle);
		obstacle.OnConveyerSpeedChanged(ConveyerSpeed);
		obstacle.SetRandomPosition(YBounds);
	}

	private void OnGenTimerTimeout()
	{
		GenerateObstacle();
		StartGenTimer();
	}

	public void OnObstacleFell()
	{
		AddScore();
	}

	private void StartGenTimer()
	{
		_genTimer.WaitTime = Math.Max(1f, _rand.Next(2,8) - ConveyerSpeed/100);
		_genTimer.Start();
	}

	public void UnsubscribeEvents()
	{
		ConveyerSpeedChanged = null;
	}

}
