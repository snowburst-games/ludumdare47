using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{
	private ScreenShaker2D _shaker;
	private Camera2D _camera;
	private SpiceGenerator _spiceGenerator;
	private ObstacleGenerator _obstacleGenerator;
	private HandGenerator _handGenerator;
	private StaticBody2D _conveyerBelt;
	private AudioStreamPlayer _musicPlayer;
	private Player _player;
	private float _conveyerSpeed = 100; // put this in conveyer script?
	private Label _lblTime;
	private Label _lblSpiciness;
	private Label _lblFriendsSaved;
	private HUD _HUD;
	private PictureStory _pictureStory;
	private float _timeElapsed = 0;
	private int _spiciness = 0;
	private int _friendsSaved = 0;
	private int _finalScore = 0;
	private AnimationPlayer _animHUD;
	private Dictionary<AudioData.WorldSounds, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);
	public int Spiciness {
		get{
			return _spiciness;
		}
		set{
			_spiciness = value;
			_lblSpiciness.Text = "Saucy: " + _spiciness;
		}
	}

	public override void _Ready()
	{
		base._Ready();
		GetTree().Paused = true;
		_spiceGenerator = GetNode<SpiceGenerator>("SpiceGenerator");
		_obstacleGenerator = GetNode<ObstacleGenerator>("ObstacleGenerator");
		_handGenerator = GetNode<HandGenerator>("HandGenerator");
		_conveyerBelt = GetNode<StaticBody2D>("ConveyerBelt");
		_HUD = GetNode<HUD>("HUD");
		_animHUD = GetNode<AnimationPlayer>("HUD/Anim");
		_HUD.BtnQuitPressed+=this.OnHUDBtnQuitPressed;
		_camera = GetNode<Camera2D>("Camera");
		_shaker = new ScreenShaker2D(_camera);
		_player = GetNode<Player>("Player");
		_player.PlayerDied+=this.OnPlayerDied;
		_player.PlayerHit+=this.OnPlayerHit;
		_lblTime = GetNode<Label>("HUD/LblTime");
		_lblSpiciness = GetNode<Label>("HUD/LblSpicy");
		_lblFriendsSaved = GetNode<Label>("HUD/LblFriends");
		_pictureStory = GetNode<PictureStory>("HUD/PictureStory");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		
		_obstacleGenerator.YBounds = _spiceGenerator.YBounds = 
			new float[2] { _conveyerBelt.Position.y, _conveyerBelt.Position.y + 212.61f };// _conveyerBelt.GetNode<Sprite>("Sprite").RegionRect.Size.y };
		_spiceGenerator.ConveyerSpeed = _conveyerSpeed;
		_obstacleGenerator.ConveyerSpeed = _conveyerSpeed;
		_handGenerator.ConveyerSpeed = _conveyerSpeed;
		_spiceGenerator.AddScore = AddScore;
		_obstacleGenerator.AddScore = OnFriendSaved;
		
		_player.ConveyerSpeed = _conveyerSpeed;
		Start();
		
	}

	public override void Init()
	{
		base.Init();

		GetTree().Paused = true;
		AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
	}

	

	public async void Start()
	{
		_animHUD.Play("Start");
		await ToSignal(_animHUD, "animation_finished");
		GetTree().Paused = false;
		_HUD.Starting = false;
	}

	public void OnPlayerHit()
	{
		_shaker.Shake(0.18f, 3);
	}

	public void OnPlayerDied()
	{
		EndGame();
	}

	private void EndGame()
	{
		_spiceGenerator.UnsubscribeEvents();
		_obstacleGenerator.UnsubscribeEvents();
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnEndGameStoryCompleted;
		_pictureStory.StayPausedOnFinish = false;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_finalScore = Convert.ToInt32(_timeElapsed)*10 + Spiciness*2 + _friendsSaved*40;
		_pictureStory.AddScreen("res://Stages/World/Art/lose_PICTURE_STORY.png",
		"\n\n\n" +
		"           " + "Time Survived: " + Convert.ToInt32(_timeElapsed) + "\n" +
		"           " + "Saucy?: " + Spiciness + "\n" +
		"           " + "You have " + _friendsSaved + " friends" + "\n" +
		"           " + "FINAL SCORE: " + (_finalScore), 
			vAlign:Label.VAlign.Center, hAlign:Label.AlignEnum.Left);
		_pictureStory.Start();
	}

	private void OnEndGameStoryCompleted()
	{
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Effects"), true);
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Voice"), true);
		GetNode("HUD/ScoreManagerMono/score_manager/leaderboard/btn_close").Connect("pressed", this, nameof(OnBtnRestartPressed));
		GetNode("HUD/ScoreManagerMono/score_manager/leaderboard/BtnMainMenu").Connect("pressed", this, nameof(OnHUDBtnQuitPressed));
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").StartScoreSaver(_finalScore, 10, true);
	}

	public void SetConveyerSpeed(float speed) //? put in conveyer script
	{
		_conveyerSpeed = speed;
		_player.ConveyerSpeed = _conveyerSpeed;
		_spiceGenerator.ConveyerSpeed = _conveyerSpeed;
		_obstacleGenerator.ConveyerSpeed = _conveyerSpeed;
		_handGenerator.ConveyerSpeed = _conveyerSpeed;
	}

	public void AddScore(int num)
	{
		Spiciness += num;
	}

	public void OnFriendSaved()
	{
		_friendsSaved += 1;
		_lblFriendsSaved.Text = "Friends: " + _friendsSaved;
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		_timeElapsed += delta;

		_lblTime.Text = "Time: " +  Convert.ToInt32(_timeElapsed);
		SetConveyerSpeed(Math.Min(400,100+Convert.ToInt32(_timeElapsed*3)));
	}

	public void OnHUDBtnQuitPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Effects"), false);
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Voice"), false);
	}

	private void OnBtnRestartPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Effects"), false);
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Voice"), false);
	}

}


