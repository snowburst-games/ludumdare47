using Godot;
using System;

public class HandGenerator : Node2D
{
	PackedScene scene;
	Node node;
	float Type = 0;
	Random random = new Random();
	Sprite sprite;
	Vector2 spritePos;
	Hand Hand;

	Timer timer;
	public delegate void ConveyerSpeedChangedDelegate(float speed);
	public event ConveyerSpeedChangedDelegate ConveyerSpeedChanged;
	private float _conveyerSpeed=1;
	public float ConveyerSpeed
	{
		get{
			return _conveyerSpeed;
		}
		set{
			_conveyerSpeed = value;
			ConveyerSpeedChanged?.Invoke(_conveyerSpeed);
		}
	}

	
	public override void _Ready()
	{
		
		timer = (Timer)GetNode("Timer");
		StartTimer();
	}



   public override void _Process(float delta)
   {
   }

   	public void AddNewHand()
	{
		scene = (PackedScene)ResourceLoader.Load("res://Actors/Hand/Hand.tscn");
		Hand hand = (Hand)scene.Instance();
		ConveyerSpeedChanged+=hand.OnConveyerSpeedChanged;
		AddChild(hand);	
		
		hand.HandDied += this.OnHandDied;
		hand.Start();

	}

	public void OnHandDied()
	{
		//AddNewHand();
	}

	private void OnTimerTimeout()
	{
		AddNewHand();
		StartTimer();
	}

	private void StartTimer()
	{
		int _spawnRateUpper = 1500/(int)_conveyerSpeed+1;
		int _spawnRateLower = 200/(int)_conveyerSpeed+1;
		GD.Print("lower spawn rate: " + _spawnRateLower + ", upper spawn rate: " + _spawnRateUpper + ", conveyer speed: " + _conveyerSpeed);
		timer.WaitTime = random.Next(_spawnRateLower,_spawnRateUpper);
		timer.WaitTime = Mathf.Clamp(timer.WaitTime, 0.5f,10);
		GD.Print(timer.WaitTime + "wt");
		timer.Start();
		//x by conveyer speed ... mathf.clamp(1,10);
	}


}






