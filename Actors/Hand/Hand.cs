using Godot;
using System;
using System.Collections.Generic;

public class Hand : Area2D
{
	public int Speed = 200;
	public Vector2 screensize;
	//Vector2 velocity = new Vector2();
	public Vector2 velocity;
	public delegate void HandDiedDelegate();
	public event HandDiedDelegate HandDied;
	public Sprite _sprite;
	public Timer hTimer;
	private HandActionState _handActionState;
	Random random = new Random();
	int _yPosUpper = -400;
	int _yPosLower = -300;
	int _xPosUpper = 900;
	int _xPosLower = -100;
	int _handType;
	private Dictionary<AudioData.HandSounds, AudioStream> _handSounds = AudioData.LoadedSounds<AudioData.HandSounds>(AudioData.HandSoundPaths);
	private AudioStreamPlayer2D _soundPlayer;

	// int flip;
	private Dictionary<int, Rect2> HandSpriteRegions = new Dictionary<int, Rect2>()
	{
		{1, new Rect2(new Vector2(500, 260), new Vector2(500, 281))},
		{2, new Rect2(new Vector2(1040, 260), new Vector2(500, 281))},
		{3, new Rect2(new Vector2(20, 581), new Vector2(500, 281))}
	};


	public enum ActionStateEnum 
	{
		Moving, Poking
	}


	public void OnConveyerSpeedChanged(float speed)
	{
		//Speed = (int)speed*2;
	}

	public void SetActionState(ActionStateEnum actionState)
	{
		switch(actionState)
		{
			case ActionStateEnum.Moving:
				_handActionState = new MovingHandActionState(this);
				break;
			case ActionStateEnum.Poking:
				_handActionState = new PokingActionState(this);
				break;
		}
	}
		
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		SetActionState(ActionStateEnum.Moving);
		velocity = new Vector2();
		screensize = GetViewport().GetSize();
		_handType = random.Next(1,4);
		_sprite = (Sprite)GetNode("Sprite");
		_sprite.RegionRect = HandSpriteRegions[_handType]; //update as per sprites
		// flip = random.Next(0,2);
		// // if (flip == 1)
		// {
		// 	_sprite.FlipH = true;
		// 	_sprite.FlipV = true;
		// }
		hTimer = (Timer)GetNode("HTimer");
		StartTimer(); 
		Start();
	}

	public void Start()
	{
		Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosUpper,_yPosLower));
	}

	public override void _PhysicsProcess(float delta)
	{
		if (_handActionState == null)
		{
			return;
		}
		
		_handActionState.Update(delta);
	
	}
	public void PlaySound(AudioData.HandSounds sound)
	{
		// if (_soundPlayer.Playing && _soundPlayer.Stream == _playerSounds[sound])
		// {
		// 	return;
		// }

		AudioHandler.PlaySound(_soundPlayer, _handSounds[sound], AudioData.SoundBus.Effects);
	}

	public void Die()
	{
		HandDied?.Invoke();	
		QueueFree();
	}

	public void StartTimer() //timer for switching from moving to poking
	{
		hTimer.WaitTime = random.Next(1,3);
		hTimer.Start();
	}

	private void OnHTimerTimeout()
	{	
		//StartTimer();
	}




	private void OnBodyEntered(Godot.Object body)
	{
		if (body is Player p)
		{
			p.OnHitByHand();
		}
	}


/*
1. random poke - done
2. Speed of hand adjust with conveyer speed - done (conveyer speed updates the speed of the hand...)
3. Speed of hand generations as above - looked weird. didnt do.
4. Random hand sprite - position dictionary
*/


}




