using Godot;
using System;

public class MovingHandActionState : HandActionState
{

	private int _forwardMove;
	Random random = new Random();
	

	public MovingHandActionState()
	{	
		

	}

	public MovingHandActionState(Hand hand)
	{
		this.Hand = hand;
		_forwardMove = random.Next(20,50);
		GD.Print("Move");
		
	}

	public override void Update(float delta)
	{
		
		base.Update(delta);
		Hand.velocity.x += _forwardMove;
		Hand.velocity.y -= 300;
		
		if (Hand.Position.x > Hand.screensize.x + Hand._sprite.RegionRect.Size.x)
		{
			Hand.Die();
		}
		// if (Hand.Position.y>-200 && Hand.Position.y<=-100)
		// {
		// 	GD.Print(">-100");
		// 	Hand.SetActionState(Hand.ActionStateEnum.Poking);
		// }
		if (Hand.hTimer.IsStopped() == false && Hand.Position.y<-200)
		{
			Hand.SetActionState(Hand.ActionStateEnum.Poking);
		}
		if (Hand.velocity.Length() > 0)
		{
			Hand.velocity = Hand.velocity.Normalized() * Hand.Speed;
		} 
		Hand.Position += Hand.velocity * delta;
		// Hand.Position = new Vector2(x: Mathf.Clamp(Hand.Position.x, 0, 2000),
		// y: Mathf.Clamp(Hand.Position.y, 0, 200));
		
	}

}
