using Godot;
using System;
using System.Collections.Generic;

public class Player : KinematicBody2D
{
	public delegate void PlayerHitDelegate();
	public event PlayerHitDelegate PlayerHit;
	public delegate void PlayerDiedDelegate();
	public event PlayerDiedDelegate PlayerDied;

	private AnimationPlayer _anim;
	private Sprite _sprite;
	public Vector2 MoveDir {get; set;}
	public float Speed {get; set;} = 500;
	public Vector2 Velocity {get; set;}
	public float ConveyerSpeed {get; set;} = 0;
	private AudioStreamPlayer2D _movementSoundPlayer;
	private AudioStreamPlayer2D _voiceSoundPlayer;
	private AudioStreamPlayer2D _effectsSoundPlayer;
	private Dictionary<AudioData.PlayerSounds, AudioStream> _playerSounds = AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);

	public enum ActionState	{ Idle, Moving, Hit, Dying }
	// public enum Anim { MoveHorizontal, MoveVertical}

	private PlayerActionState _actionState;

	public override void _Ready()
	{
		
		SetPhysicsProcess(false);
		_anim = GetNode<AnimationPlayer>("Anim");
		_sprite = GetNode<Sprite>("Sprite");
		_sprite.Scale = new Vector2(0,0);
		_movementSoundPlayer = GetNode<AudioStreamPlayer2D>("MovementSoundPlayer");
		_voiceSoundPlayer = GetNode<AudioStreamPlayer2D>("VoiceSoundPlayer");
		_effectsSoundPlayer = GetNode<AudioStreamPlayer2D>("EffectsSoundPlayer");
		ChangeState(ActionState.Idle);
		Start();		
	}

	public async void Start()
	{
		PlayAnim("Spawn");
		await ToSignal (_anim, "animation_finished");
		SetPhysicsProcess(true);
	}

	public void ChangeState(ActionState state)
	{
		switch (state)
		{
			case ActionState.Idle:
				_actionState = new IdlePlayerActionState(this);
				break;
			case ActionState.Moving:
				_actionState = new MovingPlayerActionState(this);
				break;
			case ActionState.Hit:
				_actionState = new HitPlayerActionState(this);
				break;
			case ActionState.Dying:
				_actionState = new DyingPlayerActionState(this);
				break;
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		_actionState.Update(delta);
	}

	public AudioData.PlayerSounds GetRandomHurtSound()
	{
		return (new AudioData.PlayerSounds[3] {AudioData.PlayerSounds.Aah, AudioData.PlayerSounds.Ooh, AudioData.PlayerSounds.Ow})
			[new Random().Next(0,3)];
	}
	public AudioData.PlayerSounds GetRandomCollectSound()
	{
		return (new AudioData.PlayerSounds[2] {AudioData.PlayerSounds.Collect, AudioData.PlayerSounds.Collect2})
			[new Random().Next(0,2)];
	}

	public void PlayAnim(string animName)
	{
		if (_anim.IsPlaying() && _anim.CurrentAnimation == animName)
		{
			return;
		}

		_anim.Play(animName);
	}

	public void PlayMovementSound(AudioData.PlayerSounds sound)
	{
		if (_movementSoundPlayer.Playing && _movementSoundPlayer.Stream == _playerSounds[sound])
		{
			return;
		}

		AudioHandler.PlaySound(_movementSoundPlayer, _playerSounds[sound], AudioData.SoundBus.Effects);
	}

	public void PlayVoiceSound(AudioData.PlayerSounds sound)
	{
		// if (_soundPlayer.Playing && _soundPlayer.Stream == _playerSounds[sound])
		// {
		// 	return;
		// }

		AudioHandler.PlaySound(_voiceSoundPlayer, _playerSounds[sound], AudioData.SoundBus.Voice);
	}
	public void PlayEffectsSound(AudioData.PlayerSounds sound)
	{
		// if (_soundPlayer.Playing && _soundPlayer.Stream == _playerSounds[sound])
		// {
		// 	return;
		// }

		AudioHandler.PlaySound(_effectsSoundPlayer, _playerSounds[sound], AudioData.SoundBus.Effects);
	}


	public void StopMovementSound(AudioData.PlayerSounds sound, bool voice=true)
	{
		if (_movementSoundPlayer.Playing && _movementSoundPlayer.Stream == _playerSounds[sound])
		{
			_movementSoundPlayer.Stop();
		}
	}

	public void OnHitByHand()
	{
		ChangeState(ActionState.Dying);
	}

	public void OnHitByBadSpice()
	{
		PlayerHit?.Invoke();
		ChangeState(ActionState.Hit);
	}

	public void OnHitByGoodSpice()
	{
		PlayEffectsSound(GetRandomCollectSound());
	}

	public void OnDieFinish()
	{
		PlayerDied?.Invoke();
		PlayerHit = null;
		PlayerDied = null;
	}
	


	public void FaceCorrectDirection()
	{
		_sprite.FlipH = MoveDir.x < 0 ? true : MoveDir.x > 0 ? false : _sprite.FlipH;
		// _sprite.FlipV = MoveDir.y < 0 ? false : MoveDir.y > 0 ? true : _sprite.FlipV;
	}
}
