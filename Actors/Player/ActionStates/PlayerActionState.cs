using Godot;
using System;

public class PlayerActionState
{
	public Player Player {get; set;}
	
	public virtual void Update(float delta)
	{
		Move(delta);
		Player.MoveDir = new Vector2(Input.IsActionPressed("P1Left") ? -1 : Input.IsActionPressed("P1Right") ? 1 : 0,
		Input.IsActionPressed("P1Up") ? -1 : Input.IsActionPressed("P1Down") ? 1 : 0).Normalized();
	}

	public void Move(float delta)
	{
		CalcCollisions(Player.MoveAndCollide((Player.Velocity + new Vector2(-Player.ConveyerSpeed, 0)) * delta));
		// if (Player.Velocity.Length() > 50)
		// {
		// 	Player.PlayMovementSound(AudioData.PlayerSounds.Walk);
		// }
	}

	public void CalcCollisions(KinematicCollision2D collisionData)
	{
		if (collisionData == null)
		{
			return;
		}
		if (collisionData.Collider is Obstacle obstacle)
		{
			obstacle.OnGetPushed(Player.MoveDir);
		}
		if (collisionData.Collider is StaticBody2D staticBody) // don't get stuck on the left
		{
			Player.Position = new Vector2( Player.Position.x < 100 ?Player.Position.x + 1: Player.Position.x, Player.Position.y);
		}
	}
}
