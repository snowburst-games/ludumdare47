using Godot;
using System;

public class HitPlayerActionState : PlayerActionState
{
	public override void Update(float delta)
	{
		base.Update(delta);
		Player.Velocity = new Vector2(0,0);
		FinishHit();
	}

	public async void FinishHit()
	{
		Player.PlayAnim("Hit");
		await Player.ToSignal(Player.GetNode<AnimationPlayer>("Anim"), "animation_finished");
		Player.ChangeState(Player.ActionState.Idle);
	}

	public HitPlayerActionState()
	{

	}
	public HitPlayerActionState(Player player)
	{
		this.Player = player;
		Player.PlayVoiceSound(Player.GetRandomHurtSound());
	}
}
