using Godot;
using System;

public class DyingPlayerActionState : PlayerActionState
{
	public override void Update(float delta)
	{
		base.Update(delta);
		Player.Velocity = new Vector2(0,0);
		FinishDie();
	}

	public async void FinishDie()
	{
		Player.PlayAnim("Die");
		await Player.ToSignal(Player.GetNode<AnimationPlayer>("Anim"), "animation_finished");
		Player.OnDieFinish();
	}

	public DyingPlayerActionState()
	{

	}
	public DyingPlayerActionState(Player player)
	{
		this.Player = player;
		Player.PlayVoiceSound(Player.GetRandomHurtSound());
	}
}
