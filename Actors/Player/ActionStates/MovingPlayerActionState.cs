using Godot;
using System;

public class MovingPlayerActionState : PlayerActionState
{
	public override void Update(float delta)
	{
		base.Update(delta);
		Player.FaceCorrectDirection();
		Player.Velocity = 
			new Vector2(Player.MoveDir.x > 0 ? Player.MoveDir.x * Player.Speed : Player.MoveDir.x * Player.Speed ,
			Player.MoveDir.y * Player.Speed);

		Player.PlayAnim(Player.MoveDir.x != 0 ? "MoveHorizontal" : "MoveVertical");
		Player.PlayMovementSound(AudioData.PlayerSounds.Walk);
		if (Player.MoveDir == new Vector2(0,0))
		{
			Player.StopMovementSound(AudioData.PlayerSounds.Walk);
			Player.ChangeState(Player.ActionState.Idle);
		}
	}

	public MovingPlayerActionState()
	{

	}
	public MovingPlayerActionState(Player player)
	{
		this.Player = player;
	}
}
