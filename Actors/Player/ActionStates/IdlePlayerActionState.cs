using Godot;
using System;

public class IdlePlayerActionState : PlayerActionState
{
	public override void Update(float delta)
	{
		base.Update(delta);
		Player.Velocity = new Vector2(0,0);

		if (Player.MoveDir != new Vector2(0,0))
		{
			Player.ChangeState(Player.ActionState.Moving);
		}

		Player.PlayAnim("Idle");
	}

	public IdlePlayerActionState()
	{

	}
	public IdlePlayerActionState(Player player)
	{
		this.Player = player;
	}
}
