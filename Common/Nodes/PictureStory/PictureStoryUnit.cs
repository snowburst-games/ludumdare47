using Godot;
using System;

public class PictureStoryUnit
{

	public Texture Background {get; set;}
	public string Text {get; set;}

	public AudioStream Audio {get; set;}
	public VideoStream Video {get; set;}
	public Label.VAlign TextVAlign {get; set;} = Label.VAlign.Bottom;
	public Label.AlignEnum TextHAlign {get; set;} = Label.AlignEnum.Center;
	
}
