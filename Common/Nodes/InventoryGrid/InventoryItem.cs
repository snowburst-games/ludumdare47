using Godot;
using System;

public class InventoryItem : Control
{	

	public delegate void ItemUsedDelegate(int itemID);
	// public event ItemUsedDelegate ItemUsed;

	public enum ItemClass
	{
		Turret,
		Toy,
		Food,
		PresentFox,
		Medicine
	}

	private ItemClass _itemClass;

	public void SetItemClass(ItemClass itemClass)
	{
		_itemClass = itemClass;
	}

	public ItemClass GetItemClass()
	{
		return _itemClass;
	}

	public void Init(IInventoriable itemType)
	{
		if (!(itemType is Node))
		{
			GD.Print("ERROR - item type is not a node");
			return;
		}
		Item = itemType;
		AddChild((Node)Item);

		if (Item is Node node)
		{
			foreach (Node n in node.GetChildren())
			if (n is TextureRect tex)
			{
				_itemTex = tex;
				// tex.HintTooltip = string.Format("{0}\nValue: £{1}",Item.ItemName, Cost);
			}
		}
	}

	private void UpdateTooltip()
	{
		_itemTex.HintTooltip = string.Format("{0}\nValue: £{1}",Item.ItemName, Cost);
	}

	private TextureRect _itemTex;

	public int ItemID {get; set;}

	// public int Cost {get; set;} = 1000;

	private int _cost = 1000;

	public int Cost
	{
		get 
		{
			return _cost;
		}
		set
		{
			_cost = value;
			UpdateTooltip();
		}
	}
		
			// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// if (Item == null)
		// {
		// 	Item = new Turret();
		// 	AddChild((Turret)Item);
		// }
	}

	// public override void _PhysicsProcess(float delta)
	// {
	// 	if (Item.Health <= 0)
	// 	{
	// 		Die();
	// 	}
	// }

	// private IInventoriable _item;

	public IInventoriable Item {get; set;}

	public void Use()
	{
		Item.Use();
		// ItemUsed?.Invoke(ItemID);
	}

	public void Die()
	{
		QueueFree();
	}

}
