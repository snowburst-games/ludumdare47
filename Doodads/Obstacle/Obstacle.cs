using Godot;
using System;
using System.Collections.Generic;

public class Obstacle : KinematicBody2D
{
	public delegate void ObstacleFellDelegate();
	public event ObstacleFellDelegate ObstacleFell;

	private Sprite _sprite;
	private AnimationPlayer _anim;
	private Dictionary<AudioData.ObstacleSounds, AudioStream> _obstacleSounds = AudioData.LoadedSounds<AudioData.ObstacleSounds>(AudioData.ObstacleSoundPaths);
	private AudioStreamPlayer2D _soundPlayer;
	public enum ObstacleTypeEnum {Fries, Donut, Sundae}
	private bool _dying = false;

	[Export]
	public ObstacleTypeEnum ObstacleType = ObstacleTypeEnum.Fries;

	private Dictionary<ObstacleTypeEnum, Rect2> _obstacleSpriteRegions = new Dictionary<ObstacleTypeEnum, Rect2>()
	{
		{ObstacleTypeEnum.Donut, new Rect2(new Vector2(500, 20), new Vector2(200, 200))},
		{ObstacleTypeEnum.Fries, new Rect2(new Vector2(740, 20), new Vector2(200, 200))},
		{ObstacleTypeEnum.Sundae, new Rect2(new Vector2(260, 260), new Vector2(200, 200))}
	};

	public float Speed {get; set;} = 0;
	private Vector2 _pushForce {get; set;}
	private Vector2 _pushDir;

	private float[] _yBounds {get; set;}
	
	public void OnConveyerSpeedChanged(float speed)
	{
		Speed = speed;
	}

	public override void _Ready()
	{
		base._Ready();
		_sprite = GetNode<Sprite>("Sprite");
		_anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_sprite.RegionRect = _obstacleSpriteRegions[ObstacleType];

		switch (ObstacleType)
		{
			case ObstacleTypeEnum.Fries:
				GetNode<CollisionShape2D>("ShapeFries").Disabled = false;
				break;
			case ObstacleTypeEnum.Donut:
				GetNode<CollisionShape2D>("ShapeDonut").Disabled = false;
				break;
			case ObstacleTypeEnum.Sundae:
				GetNode<CollisionShape2D>("ShapeSundae").Disabled = false;
				break;
		}
		
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		CalcCollisions(MoveAndCollide(
			(new Vector2( -Speed, 0) + _pushForce) * delta
			, false));


		_pushForce = new Vector2(0,0);
		if (Position.x + _sprite.RegionRect.Size.x < 0)
		{
			ObstacleFell = null;
			QueueFree();
		}

		if (_yBounds == null)
		{
			return;
		}

		if (Position.y < _yBounds[0] - (int)(0.5f*_sprite.RegionRect.Size.y)/3f || Position.y  > _yBounds[1] + (int)(0.5f*_sprite.RegionRect.Size.y)/3f)
		{
			if (!_dying)
			{
				Die();
			}
		}
	}

	public void OnGetPushed(Vector2 fromDir)
	{
		_pushDir = fromDir;
		_pushForce = Speed * 1.1f > 400 ? Speed * 1.1f * fromDir : 400 * fromDir;
	}
	
	public AudioData.ObstacleSounds GetRandomYaySound()
	{
		return (new AudioData.ObstacleSounds[4] {AudioData.ObstacleSounds.Hehehe, AudioData.ObstacleSounds.ThankYou, AudioData.ObstacleSounds.Woohoo, AudioData.ObstacleSounds.Yay})
			[new Random().Next(0,4)];
	}

	public async void Die()
	{
		_dying = true;
		AudioHandler.PlaySound(_soundPlayer, _obstacleSounds[GetRandomYaySound()], AudioData.SoundBus.Voice);
		_anim.Play("Die");
		ObstacleFell?.Invoke();
		ObstacleFell = null;

		await ToSignal(_anim, "animation_finished");

		QueueFree();
	}

	public void CalcCollisions(KinematicCollision2D collisionData)
	{
		if (collisionData == null)
		{
			return;
		}

		if (collisionData.Collider is Obstacle obstacle)
		{
			if (_pushForce != new Vector2(0,0))
			{
				obstacle.OnGetPushed(_pushDir);
			}

		}
	}

	public void SetRandomPosition(float[] yBounds)
	{
		_yBounds = yBounds;
		GD.Print(_yBounds[0], ", ", _yBounds[1]);
		Position = new Vector2(GetViewportRect().Size.x + _sprite.RegionRect.Size.x, 
			new Random().Next((int)yBounds[0] + (int)(0.2f*_sprite.RegionRect.Size.y), (int)yBounds[1] - (int)(0.2f*_sprite.RegionRect.Size.y)));
	}

}
