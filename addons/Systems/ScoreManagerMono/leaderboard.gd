extends Panel

var pnl_scoreunit_scn = preload("res://addons/Systems/ScoreManagerMono/pnl_scoreunit.tscn")

var max_scores_to_show = 10

func _ready():
	visible = false
	$loading_sprite.visible = false

func show_and_refresh_board(max_scores):
	show_board()
	refresh_board(max_scores)

func refresh_board(max_scores):
	max_scores_to_show = max_scores
	clear_board()
	$loading_sprite.visible = true
	$anim.play("Loading")
	yield(SilentWolf.Scores.get_high_scores(max_scores), "sw_scores_received")
	$loading_sprite.visible = false
	var pnl_size = $pnl_scores.rect_size.y
	var increment = round(pnl_size/max_scores)
	for n in range(max_scores):
		if SilentWolf.Scores.scores.size() < n+1:
			return
		var item = SilentWolf.Scores.scores[n]
		var pnl_scoreunit = pnl_scoreunit_scn.instance()
		$pnl_scores.add_child(pnl_scoreunit)
		pnl_scoreunit.rect_position.y = increment*n
		pnl_scoreunit.rect_size = Vector2($pnl_scores.rect_size.x,increment)
		pnl_scoreunit.get_node("lbl_name").rect_size.y = increment
		pnl_scoreunit.get_node("lbl_pos").rect_size.y = increment
		pnl_scoreunit.get_node("lbl_score").rect_size.y = increment
		
		pnl_scoreunit.get_node("lbl_name").text = item["player_name"]
		pnl_scoreunit.get_node("lbl_pos").text = str(n+1)+"."
		pnl_scoreunit.get_node("lbl_score").text = str(item["score"])

func show_board():
	visible = true

func _on_btn_close_pressed():
	visible = false
	$loading_sprite.visible = false

func clear_board():
	for node in $pnl_scores.get_children():
		node.queue_free()

func _on_btn_refresh_pressed():
	refresh_board(max_scores_to_show)
