extends Node

var config = {
	"api_key": "",
	"game_id": "",
	"game_version": "1.0.2",
	"log_level": 1
}

func _ready():
	if SilentWolf.config != config:
		SilentWolf.configure(config)
