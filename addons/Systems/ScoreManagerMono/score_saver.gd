extends Panel

signal go_to_leaderboard
var show_leaderboard_on_finish = true

var high_score = 0
var max_scores_to_show = 10

func _ready():
	visible = false
	$cnt_highscore.visible = false
	$cnt_checking.visible = false
	$cnt_highscore/lbl_status.visible = false

func start(score, max_scores, show_leaderboard):
	high_score = score
	max_scores_to_show = max_scores
	show_leaderboard_on_finish = show_leaderboard
	visible = true
	$cnt_checking.visible = true
	$cnt_checking/anim.play("Loading")
	yield(SilentWolf.Scores.get_score_position(score), "sw_position_received")
	$cnt_checking.visible = false
	var position = SilentWolf.Scores.position
	if position <= max_scores:
		$cnt_highscore.visible = true
		$cnt_highscore/lbl_title.text += str(high_score) + "!"
	else:
		visible = false
		if show_leaderboard_on_finish:
			emit_signal("go_to_leaderboard", max_scores_to_show)
		
func is_valid_name(name):
	if name == "":
		return false
	return true


func _on_BtnSubmit_pressed():
	if !is_valid_name($cnt_highscore/led_name.text):
		$cnt_highscore/lbl_status.visible = true
		$cnt_highscore/lbl_status.text = "Invalid player name"
		return
	$cnt_highscore/lbl_status.visible = false
	$cnt_highscore/btn_submit.disabled = true
	$cnt_highscore/anim.play("Loading")
	yield(SilentWolf.Scores.persist_score($cnt_highscore/led_name.text, high_score), "sw_score_posted")
	$cnt_highscore.visible = false
	visible = false
	if show_leaderboard_on_finish:
		emit_signal("go_to_leaderboard", max_scores_to_show)

func _input(event):
	if Input.is_key_pressed(KEY_ENTER) and (event.is_pressed() and not event.is_echo()) and $cnt_highscore.visible:
		_on_BtnSubmit_pressed()
