extends Node


func _ready():
	$score_saver.connect("go_to_leaderboard", $leaderboard, "show_and_refresh_board")

func wipe():
	SilentWolf.Scores.wipe_leaderboard()
