using Godot;
using System;
using System.Collections.Generic;

public class Spice : Area2D
{

	public delegate void SpiceTouchedPlayerDelegate(int score);
	public event SpiceTouchedPlayerDelegate SpiceTouchedPlayer;

	private Sprite _sprite;
	private AnimationPlayer _anim;

	public enum SpiceTypeEnum {Chilli, Ketchup, Mayo, Knife1, Knife2, Knife3}

	[Export]
	public SpiceTypeEnum SpiceType = SpiceTypeEnum.Chilli;

	private Dictionary<SpiceTypeEnum, Rect2> _spiceSpriteRegions = new Dictionary<SpiceTypeEnum, Rect2>()
	{
		{SpiceTypeEnum.Chilli, new Rect2(new Vector2(260, 20), new Vector2(200, 200))},
		{SpiceTypeEnum.Ketchup, new Rect2(new Vector2(980, 20), new Vector2(200, 200))},
		{SpiceTypeEnum.Mayo, new Rect2(new Vector2(20, 260), new Vector2(200, 200))},
		{SpiceTypeEnum.Knife1, new Rect2(new Vector2(1220, 20), new Vector2(200, 200))},
		{SpiceTypeEnum.Knife2, new Rect2(new Vector2(1460, 20), new Vector2(200, 200))},
		{SpiceTypeEnum.Knife3, new Rect2(new Vector2(1700, 20), new Vector2(200, 200))},
	};
	private Dictionary<SpiceTypeEnum, int> _spiceScores = new Dictionary<SpiceTypeEnum, int>()
	{
		{SpiceTypeEnum.Chilli, 10},
		{SpiceTypeEnum.Ketchup, 15},
		{SpiceTypeEnum.Mayo, 20},
		{SpiceTypeEnum.Knife1, -20},
		{SpiceTypeEnum.Knife2, -15},
		{SpiceTypeEnum.Knife3, -10}
	};

	private int _score = 0;
	public float _speed = 0;
	
	public void OnConveyerSpeedChanged(float speed)
	{
		_speed = speed;
	}

	public override void _Ready()
	{
		base._Ready();
		_sprite = GetNode<Sprite>("Sprite");
		_anim = GetNode<AnimationPlayer>("Anim");

		_score = _spiceScores[SpiceType];
		_sprite.RegionRect = _spiceSpriteRegions[SpiceType];

		if (_score < 0)
		{
			GetNode<CollisionShape2D>("Shape").Disabled = true;
			GetNode<CollisionShape2D>("ShapeBad").Disabled = false;
		}
		
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		Position = new Vector2(Position.x - (_speed * delta), Position.y);

		if (Position.x + _sprite.RegionRect.Size.x < 0)
		{
			UnsubscribeEvents();
			QueueFree();
		}
	}

	public void SetRandomPosition(float[] yBounds)
	{
		GD.Print(yBounds[0], ", ", yBounds[1]);
		Position = new Vector2(GetViewportRect().Size.x + _sprite.RegionRect.Size.x, 
			new Random().Next((int)yBounds[0] + (int)(0.2f*_sprite.RegionRect.Size.y), (int)yBounds[1] - (int)(0.2f*_sprite.RegionRect.Size.y)));
	}

	private void OnBodyEntered(Godot.Object body)
	{
		if (body is Player player)
		{
			SpiceTouchedPlayer?.Invoke(_score);
			if (_score < 0)
			{
				player.OnHitByBadSpice();
			}
			else
			{
				player.OnHitByGoodSpice();
			}
			Die();
		}
	}

	public void UnsubscribeEvents()
	{
		SpiceTouchedPlayer = null;
	}

	public async void Die()
	{
		UnsubscribeEvents();
		_anim.Play("Die");

		await ToSignal(_anim, "animation_finished");

		QueueFree();
	}

}
