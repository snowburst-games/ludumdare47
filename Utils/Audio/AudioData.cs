// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};


	public enum MainMenuSounds {
		Music
	};
	public enum WorldSounds {
		Music
	};

	public enum ObstacleSounds {
		Hehehe,
		ThankYou,
		Woohoo,
		Yay
	};

	public enum HandSounds {
		Fwoop
	};

	public enum PlayerSounds {
		Aah,
		Ooh,
		Ow,
		Walk,
		Collect,
		Collect2
	};


	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/BurgerMenuLow.ogg"}
	};

	public static Dictionary<WorldSounds, string> WorldSoundPaths = new Dictionary<WorldSounds, string>()
	{
		{WorldSounds.Music, "res://Stages/World/Music/SillyBurgerLowPitch.ogg"}
	};
	public static Dictionary<ObstacleSounds, string> ObstacleSoundPaths = new Dictionary<ObstacleSounds, string>()
	{
		{ObstacleSounds.Hehehe, "res://Doodads/Obstacle/Sounds/Hehehe.wav"},
		{ObstacleSounds.ThankYou, "res://Doodads/Obstacle/Sounds/ThankYou.wav"},
		{ObstacleSounds.Woohoo, "res://Doodads/Obstacle/Sounds/Woohoo1.wav"},
		{ObstacleSounds.Yay, "res://Doodads/Obstacle/Sounds/Yay.wav"},
	};
	public static Dictionary<HandSounds, string> HandSoundPaths = new Dictionary<HandSounds, string>()
	{
		{HandSounds.Fwoop, "res://Actors/Hand/Sounds/Fwoop.wav"}
	};
	public static Dictionary<PlayerSounds, string> PlayerSoundPaths = new Dictionary<PlayerSounds, string>()
	{
		{PlayerSounds.Aah, "res://Actors/Player/Sounds/Aah.wav"},
		{PlayerSounds.Ooh, "res://Actors/Player/Sounds/Ooh.wav"},
		{PlayerSounds.Ow, "res://Actors/Player/Sounds/Ow.wav"},
		{PlayerSounds.Walk, "res://Actors/Player/Sounds/Walk.wav"},
		{PlayerSounds.Collect, "res://Actors/Player/Sounds/NiceFound1.wav"},
		{PlayerSounds.Collect2, "res://Actors/Player/Sounds/FoundNice2.wav"}
	};

	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
